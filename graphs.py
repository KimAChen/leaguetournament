import csv
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

ranks = []
positions = []

with open('LeagueTournamentPlayers.csv') as file:
  reader = csv.reader(file, delimiter=',')
  next(reader, None)
  for row in reader:
    ranks.append(int(row[3]))
    positions.append(int(row[5]))

sns.distplot(ranks, kde=False, rug=True)
plt.xlabel('Ranks')
plt.ylabel('Frequency')
plt.savefig('RanksDist.pdf')

plt.clf()

sns.distplot(positions, bins=5, kde=False)
plt.xlabel('Position')
plt.xticks([1,2,3,4,5])
plt.ylabel('Frequency')
plt.savefig('PositionDist.pdf')
