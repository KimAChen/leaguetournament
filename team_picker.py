import argparse
import copy
import csv
from itertools import zip_longest
import random
from random import randint
import sys

num_teams = 0
preferences = []

def even_players(filename):
  with open(filename) as file:
    total = sum(1 for line in file)
  if (total-1) % 5 == 0:
    return True
  return False

def player_preferences(filename):
  global preferences
  with open(filename) as file:
    reader = csv.reader(file, delimiter=',')
    for row in reader:
      allies = []
      for value in row:
        allies.append(value)
      preferences.append(allies)

# Create player list from csv
def players_list(filename):
  players = []
  with open(filename) as file:
    reader = csv.reader(file, delimiter=',')
    next(reader, None)
    for row in reader:
      players.append((row[0], row[1], row[2], int(row[3]), row[4], int(row[5]), row[6]))
  set_num_teams(len(players))
  return players

def set_num_teams(num_players):
  global num_teams
  num_teams = int(num_players/5)

def create_initial_teams(players):
  global num_teams
  teams = []
  
  for i in range(num_teams):
    teams.append([])
  
  for i in range(len(players)):
    teams[i % num_teams].append(players[i])

  for team in teams:
    sort_team(team)
  return teams

# Sort team lowest->highest rank
def sort_team(team):
  team.sort(key=lambda tup:tup[3])
  return team

# Randomly select 2 teams, randomly select n players to swap
def mutate(teams):
  random_teams = random.sample(range(0, num_teams), 2) #randomly select 2 teams
  
  team1 = teams[random_teams[0]]
  team2 = teams[random_teams[1]]

  for index in sorted(random_teams, reverse=True):
    del teams[index]

  swap_players = randint(0, 3)
  if(swap_players == 0):
    swap1 = [team1[0]]
    del team1[0]
    swap2 = [team2[0]]
    del team2[0]
  else:
    swap1 = team1[:swap_players]
    del team1[:swap_players]
    swap2 = team2[:swap_players]
    del team2[:swap_players]

  teams.append(sort_team(team1 + swap2))
  teams.append(sort_team(team2 + swap1))

  return teams

def fitness(teams):
    return fitness_positions(teams) * fitness_rank(teams)

# fitness of positions should be as close to 3 as possible
def fitness_positions(teams):
    individual_averages = []
    for team in teams:
        total = 0
        for player in team:
            total += player[5]
        individual_averages.append(total/5)

    delta_sum = 0
    for average in individual_averages:
        delta_sum += abs(average - 3)
    delta_mean = delta_sum/len(individual_averages)

    return delta_mean

# fitness of rank should be as close to 0 as possible
def fitness_rank(teams):
    individual_averages = []
    for team in teams:
      total = 0
      for player in team:
        total += get_normalized_value(player[3])
      individual_averages.append(total/5)

    total_average = sum(individual_averages)/len(individual_averages)

    delta_sum = 0
    for average in individual_averages:
        delta_sum += abs(average - total_average)
    delta_mean = delta_sum/len(individual_averages)

    return delta_mean

table = [0, 1, 2, 3, 869.5, 929.5, 989.5, 1054.5, 1179.5, 1244.5, 1314.5, 1379.5, 1509.5, 1569.5, 1629.5, 1689.5, 1809.5, 1869.5, 1929.5, 1989.5, 2129.5]

MIN_NORM = 4
MAX_NORM = 20 - MIN_NORM
MIN_MMR = 869.5
MAX_MMR = 2129.5

def get_normalized_value(rank):
    global table

    mmr = table[rank]
    top = mmr - MIN_MMR
    bot = MAX_MMR - MIN_MMR
    norm = top / bot
    return (norm * MAX_NORM) + MIN_NORM

def find_team_of_player(teams, member):
  index = -1
  for ind, team in enumerate(teams):
    for player in team:
      username = player[0]
      if member == username:
        index = ind
        break
  return index

def fitness_preferences(teams):
  global preferences

  if not preferences:
    return 1.0
  
  success = 0
  for allies in preferences:
    indices = []
    for ally in allies:
      indices.append(find_team_of_player(teams, ally))
    if len(set(indices)) == 1 and -1 not in set(indices):
      success += 1

  failure = len(preferences) - success
  if failure == 0:
    return 0.1
  
  fitness = failure/len(preferences)
  return fitness

def generation(mom, dad, population):
    children = []
    for i in range(population):
        children.append(copy.deepcopy(mom))
    for i in range(population):
        children.append(copy.deepcopy(dad))

    for i in range(len(children)):
        children[i] = mutate(children[i])

    children.sort(key=fitness)
    top1 = children[0]
    top2 = children[1]

    potentials = [mom, dad, top1, top2]
    potentials.sort(key=fitness)
    print("Generation top 2: " + str(fitness(potentials[0])) + ", " + str(fitness(potentials[1])))
    return (potentials[0], potentials[1])

def genetics(initialList, numGenerations, numPopulation):
    initialClone = mutate(copy.deepcopy(initialList))

    top1 = initialList
    top2 = initialClone

    for i in range(numGenerations):
        print("Generation: " + str(i))
        topOfGeneration = generation(top1, top2, numPopulation)
        top1 = topOfGeneration[0]
        top2 = topOfGeneration[1]
    return top1

def print_team(team):
    print('Team:\n')
    for player in team:
        print('{}'.format(player))
    print('\n')

def save_teams(teams, output_file):
  number = 1
  with open(output_file, 'w') as file:
    out=csv.writer(file)
    for team in teams:
      out.writerow(['Team {}'.format(number)])
      out.writerow(['Summoner', 'Discord', 'S8 Rank', 'Rank Value', 'Primary', 'Primary Position Value', 'Secondary'])
      for player in team:
        out.writerow(player)
      number+=1
      out.writerow('\n')

def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('-f', help='Name of file with player info')
  parser.add_argument('-o', nargs='?', default='TournamentTeams.csv', help='Name of output file (default: TournamentTeams.csv)')
  parser.add_argument('-p', nargs='?', type=int, default=50, help='Population*2 for each generation (default: 50)')
  parser.add_argument('-g', nargs='?', type=int, default=100, help='Number of generations default: 100)')
  parser.add_argument('-pref', nargs='?', default=None, help='Csv with player preferences')
  args = parser.parse_args()
  
  if not even_players(args.f):
    sys.exit('Uneven number of players in file. Total number of players must be a multiple of 5.')
  
  if args.pref is not None:
    player_preferences(args.pref)
  
  players = players_list(args.f) #list of players
  teams = create_initial_teams(players) #initial list of sorted teams

  final_teams = genetics(teams, args.g, args.p)
  print(str(fitness(final_teams)))
  save_teams(final_teams, args.o)

if __name__=="__main__":
  main()

